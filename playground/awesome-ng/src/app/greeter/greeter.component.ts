import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-greeter',
  template: `
    <h2 *ngIf="myTitle.length > 2">
      {{message}} BSI {{ title }}
    </h2>
    <!--    <input type="text" [(ngModel)]="title"/>-->
    <input #titleInput type="text" [value]="title"
           (keyup)="changeName(titleInput.value)"/>
    <button (click)="changeName2()" [disabled]="isDisabled">Click Me</button>

    <div>
      <ul>
        <li *ngFor="let name of names">{{name}}</li>
      </ul>
    </div>

  `,
  styleUrls: ['./greeter.component.css']
})
export class GreeterComponent {

  @Input() message!: string;
  @Output() alert = new EventEmitter<string>();

  myTitle = 'BSI';
  isDisabled = false;
  names = ['Jonas', 'Bandi', 'Gugus', 'Test'];

  get title(): string {
    return this.myTitle;
  }

  set title(value: string) {
    this.myTitle = value;
  }

  constructor() {
    // console.log('Test');
    // setInterval(() => {
    //   this.myTitle = new Date().toISOString();
    //   this.isDisabled = !this.isDisabled;
    // }, 1000);
  }

  changeName(newTitle: string): void {
    this.myTitle = newTitle;
  }

  changeName2(): void {
    this.alert.emit('Gugus');
  }

}
