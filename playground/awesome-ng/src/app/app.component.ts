import {Component} from '@angular/core';

const greeting = 'Hello';

@Component({
  selector: 'app-root',
  template: `
    <div>
      <app-greeter [message]="greeting" (alert)="onAlert($event)"></app-greeter>
      <app-greeter [message]="greeting2"></app-greeter>
    </div>
  `,
  styles: [`h2 {
    color: red;
  }`]
})
export class AppComponent {
  greeting = 'Greeting';
  greeting2 = 'Gugus';

  constructor() {
    setInterval(() => {
      this.greeting = new Date().toISOString();
    }, 1000);
  }

  onAlert(alertMessage: string): void {
    alert('HEY HEY HEY!' + alertMessage);
  }
}

