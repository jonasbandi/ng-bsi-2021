import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { ToDo } from '../../model/todo.model';

@Component({
  selector: 'td-todo-list',
  template: `
      <ul class="todo-list">
        <li *ngFor="let todo of todos">
          {{todo.title}}
          <button (click)="onRemoveToDo(todo)" class="remove-button">
            x
          </button>
        </li>
      </ul>
  `,
  styles: [
  ]
})
export class TodoListComponent implements OnInit {

  @Input() todos!: ToDo[] | undefined;
  @Output() removeToDo = new EventEmitter<ToDo>()

  constructor() { }

  ngOnInit(): void {
  }

  onRemoveToDo(todo: ToDo): void {
    this.removeToDo.emit(todo);
  }
}
