import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ToDo } from '../../model/todo.model';

@Component({
  selector: 'td-new-todo',
  template: `
    <div class="new-todo">
      <input type="text" placeholder="What needs to be done?"
             [(ngModel)]="newToDoTitle"
             autofocus autocomplete="off"/>

      <button (click)="onAddToDo()" class="add-button" id="add-button">
        +
      </button>
    </div>
  `,
})
export class NewTodoComponent implements OnInit {

  @Output() addToDo = new EventEmitter<ToDo>();
  newToDoTitle = '';

  constructor() { }

  ngOnInit(): void {
  }

  onAddToDo(): void {
    const newToDo = new ToDo(this.newToDoTitle);
    this.addToDo.emit(newToDo);
    this.newToDoTitle = '';
  }
}
