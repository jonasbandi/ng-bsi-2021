import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ToDo } from './todo';

const BACKEND_URL = 'http://localhost:3456/todos';

@Component({
  template: `
    Make sure that the todo server is running!
    <hr>
    <div class="demo-data">
      <div>
        <span>Id:</span>
        <input type="text" [(ngModel)]="todoId"/>
      </div>
      <div>
        <span>Todo Text:</span>
        <input type="text" [(ngModel)]="todoText"/>
      </div>
      <div>
        <span>Completed:</span>
        <input type="checkbox" [(ngModel)]="completed"/>
      </div>
      <div>
        <button (click)="getTodos()">Get</button>
        <button (click)="postTodo()">POST</button>
        <button (click)="putTodo()">PUT</button>
        <button (click)="deleteTodo()">DELETE</button>
      </div>
      <div>
        <pre>{{todos | json}}</pre>
        <!--<pre>{{todos$ | async | json}}</pre>-->
      </div>
    </div>
  `,
  styles: [`.demo-data div { padding: 5px} .demo-data span {width: 100px; display: inline-block}`]
})
export class BackendCrudComponent {
  todos$: Observable<ToDo[]> | undefined;
  todos: ToDo[] = [];
  todoId: number | undefined;
  todoText = '';
  completed = false;

  constructor(private http: HttpClient) {
  }

  getTodos(): void {
    this.todos$ = this.http.get<ToDo[]>(BACKEND_URL);
    this.todos$.subscribe(v => (this.todos = v));
  }

  postTodo(): void {
    const todo = {title: this.todoText, completed: this.completed};
    this.http.post(BACKEND_URL, todo).subscribe(v => console.log(v));
  }

  putTodo(): void {
    const rating = {title: this.todoText, completed: this.completed};
    this.http.put(`${BACKEND_URL}/${this.todoId}`, rating).subscribe();
  }

  deleteTodo(): void {
    this.http.delete(`${BACKEND_URL}/${this.todoId}`).subscribe();
  }
}
