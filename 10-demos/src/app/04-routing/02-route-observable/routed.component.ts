import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

type SwapiPerson = {
  name: string;
};

@Component({
  selector: 'aw-routed',
  template: `
    <h3>Routed Component</h3>
    <div>Current Detail: {{ detailNo }}</div>
    <div>Star Wars Character: {{ characterName$ | async }}</div>
    <br />
    <button (click)="goToNextDetail()">Go to next detail ...</button>
  `
})
export class RoutedObservableComponent implements OnInit {
  detailNo: number | undefined;
  characterName$!: Observable<string>;

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {}

  ngOnInit(): void {
    this.route.paramMap.pipe(map(p => parseInt(p.get('detailNumber') ?? '1', 10))).subscribe(n => (this.detailNo = n));

    this.characterName$ = this.route.paramMap.pipe(
      map(p => parseInt(p.get('detailNumber') ?? '1', 10)),
      switchMap(n => this.http.get<SwapiPerson>(`https://swapi.dev/api/people/${n}/`)),
      map(p => p.name)
    );
  }

  goToNextDetail(): void {
    const nextNumber = (this.detailNo ?? 0) + 1;

    // Navigate with relative link
    this.router.navigate(['..', nextNumber], { relativeTo: this.route });
  }
}
