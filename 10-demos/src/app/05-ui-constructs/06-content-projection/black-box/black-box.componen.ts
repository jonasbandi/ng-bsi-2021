import {
  Component,
  ContentChildren,
  QueryList,
  AfterContentInit
} from '@angular/core';
import { ClockComponent } from '../clock/clock.component';

@Component({
  selector: 'aw-black-box',
  templateUrl: 'black-box.component.html',
  styleUrls: ['black-box.component.css']
})
export class BlackBoxComponent implements AfterContentInit {
  private formatValue = 'HH:mm:ss';
  @ContentChildren(ClockComponent) children!: QueryList<ClockComponent>;

  ngAfterContentInit(): void {
    this.setFormatOnClocks();
  }

  set format(value: string) {
    this.formatValue = value;
    this.setFormatOnClocks();
  }

  get format(): string {
    return this.formatValue;
  }

  private setFormatOnClocks(): void {
    this.children.forEach(c => (c.format = this.formatValue));
  }
}
