import {Component} from '@angular/core';

@Component({
  selector: 'aw-content-main',
  template: `
    <aw-black-box>
      <aw-clock></aw-clock>
      <div>I am nested!</div>
      <aw-clock></aw-clock>
    </aw-black-box>
  `
})
export class ContentMainComponent {}

