import { Component, OnInit, ViewChild, ContentChild, ViewChildren, QueryList, AfterViewInit } from '@angular/core';
import { IPerson } from './person';

@Component({
  template: `
    <div class="parent-header">
      <span> Parent </span>
      <button (click)="childForm.reset()">Reset Forms</button>
    </div>

    <div class="parent-section-form">
      <aw-child-form #childForm (addPerson)="onAddPerson($event)"></aw-child-form>
    </div>
    <div class="parent-section-list">
      <aw-child-list [children]="persons"></aw-child-list>
    </div>
  `,
  styleUrls: ['parent.component.scss']
})
export class ParentComponent implements OnInit {
  persons: IPerson[] = [];

  ngOnInit(): void {
    this.persons = [{firstName: 'John', lastName: 'Doe', age: 42}]; // could be loaded from API ...
  }

  onAddPerson(person: IPerson): void {
    this.persons.push(person);
  }
}

// DEMO:
// Introduce `@ViewChild('childForm') childForm -> call reset() on this instance from a component method
// Delegate click to component method
// ViewChild is available in ngAfterContentInit() lifecycle-hook.
// Alternative: use `@ViewChild(ChildFormComponent) childForm` and remove the #childForm template reference variable
// Add a second child-form
// change to @ViewChildren + QueryList<ChildFormComponent> -> reset each
