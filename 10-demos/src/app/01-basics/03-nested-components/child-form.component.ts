import { Component, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { IPerson } from './person';

@Component({
  selector: 'aw-child-form',
  template: `
    <div class="child-form-header">
      <span class="title">Child Form</span>
      <span class="info">Added: {{addCount}}</span>
    </div>

    <div class="form-wrapper">
      <div> First Name: </div>
      <input type="text" [(ngModel)]="firstName">
      <div> Last Name: </div>
      <input type="text" [(ngModel)]="lastName">
      <div> Age: </div>
      <input type="number" [(ngModel)]="age">
      <button (click)="onAddPerson()">Add</button>
    </div>`,
  styleUrls: ['./child-form.component.scss']
})
export class ChildFormComponent {
  firstName = 'Tyler';
  lastName = 'Durden';
  age: number | undefined = 42;
  addCount = 0;

  @Output() addPerson = new EventEmitter<IPerson>();

  onAddPerson(): void {
    if (this.firstName && this.lastName && this.age) {
      const newPerson: IPerson = {
        firstName: this.firstName,
        lastName: this.lastName,
        age: this.age
      };
      this.addPerson.emit(newPerson);
      this.addCount++;
    }
  }

  reset(): void {
    this.addCount = 0;
  }
}
