import { Component } from '@angular/core';

@Component({
  selector: 'aw-renderless-export-as-parent',
  template: `
    <h3>
      Mouse Tracker:
    </h3>
    <aw-mouse-tracker></aw-mouse-tracker>
  `,
  styleUrls: ['./renderless-export-as-parent.component.scss']
})
export class RenderlessExportAsParentComponent {
  constructor() {}

  ngOnInit(): void {}
}
