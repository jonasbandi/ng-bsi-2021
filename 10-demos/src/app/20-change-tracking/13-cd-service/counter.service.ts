import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class CounterService {
  private _count = 0; //tslint:disable-line
  private counterHandle: number | undefined;

  get count(): number {
    return this._count;
  }

  increment(): void {
    this._count++;
    console.log('Service: counter increased: ' + this._count);
  }

  start(): void {
    this.counterHandle = window.setInterval(() => this.increment(), 1000);
  }

  stop(): void {
    clearInterval(this.counterHandle);
  }
}
